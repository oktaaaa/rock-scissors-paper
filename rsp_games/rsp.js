
const playerRock = document.getElementById('player-rock')
const playerScissors = document.getElementById('player-scissors')
const playerPaper = document.getElementById('player-paper')

const comRock = document.getElementById('com-rock')
const comScissors = document.getElementById('com-scissorrs')
const comPaper = document.getElementById('com-paper')

const playerChoices = document.querySelectorAll('.player-choice')
const refreshButton = document.getElementById('refresh-button')




let isActive = true

playerChoices.forEach((choice) => {
  choice.addEventListener('click', () => {
    
    const selected = choice.classList.contains('active')
    playerChoices.forEach(choice =>{
      choice.classList.remove('active')
    })
    if(!selected){
      choice.classList.add('active')
    }
    const playerSelection = choice
    const compSelection = compPlay()
    playRound(playerSelection,compSelection)
  })
})

function compPlay(){
    
    const choices = [comRock, comScissors, comPaper];
    const compChoices = choices[Math.floor(Math.random() * 3)];
    return compChoices
}



const result = document.querySelector('.result-image')


function playRound(playerSelection, compSelection){    
    if(playerSelection === compSelection){
      result.src ="assets/draw.png"
      return;
    } 

    if ( playerSelection === playerRock){
        if(compSelection === comScissors){
          result.src ="assets/player_win.png" 
        } else {
          result.src ="assets/com_win.png"
        }  
        
    } else if (playerSelection === playerScissors){
        if(compSelection === comPaper){
          
          result.src ="assets/player_win.png"

        } else {
          result.src ="assets/com_win.png"
        }
        
    } else if(playerSelection === playerPaper){
        if(compSelection === comRock){
          
          result.src ="assets/player_win.png"

        } else {
          result.src ="assets/com_win.png"
        
        }
    }

}


refreshButton.addEventListener('click', () => {
  window.location.reload()
})